import 'package:cloud_firestore/cloud_firestore.dart';

class MessageModel {
  String id;
  String senderId;
  String userNotificationToken;
  String mCode;
  String rType;
  String message;
  Timestamp timestamp;
  String context;
  List<String> fileUrls;
  String summary;
  String email;

  MessageModel(
      {this.id = "",
      this.senderId = '',
      this.userNotificationToken = '',
      this.mCode = "1",
      this.rType = "1",
      this.message = '',
      required this.timestamp,
      this.context = '',
      this.fileUrls = const [],
      this.summary = '',
      this.email=''});

  factory MessageModel.fromFirestore(DocumentSnapshot data) {
    Map<String, dynamic> mapData = data.data() as Map<String, dynamic>;

    return MessageModel(
        id: mapData['id'] ?? '',
        senderId: mapData['senderId'] ?? 'QxCredit',
        userNotificationToken: mapData['userNotificationToken'] ?? '',
        mCode: mapData['mCode'] ?? "1",
        rType: mapData['rType'] ?? "1",
        message:
            mapData['message'] ?? "I have a query regarding my purchased token",
        timestamp: mapData['timestamp'] ?? Timestamp.now(),
        context: mapData['context'] ?? ' empty context ',
        fileUrls: mapData['fileUrls'] == null
            ? List<String>.from([])
            : List<String>.from(mapData['fileUrls']),
        summary: mapData['summary'] ?? 'no summary to show...',
        email: mapData['email']?? '');
  }

  Map<String, dynamic> getMap() {
    return {
      'id': id,
      'senderId': senderId,
      'userNotificationToken': userNotificationToken,
      'mCode': mCode,
      'rType': rType,
      'message': message,
      'timestamp': timestamp,
      'context': context,
      'fileUrls': fileUrls,
      'summary': summary,
      'email':email,
    };
  }
}
