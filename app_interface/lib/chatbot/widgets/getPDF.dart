import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';

Future<Document> getPDF(summary) async {
  print("--------processing sections---");
  List<String> sections = summary.split("\n");
  sections = sections.sublist(0, sections.length - 1);
  List<String> headings = [];
  List<String> bodies = [];
  for (int i = 1; i < sections.length; i++) {
    String section = sections[i];
    var hbs = section.split(":");
    if (hbs.length != 2 || hbs[1].trim() == "") {
      print("-------num-${i} section skipped---------");
      continue;
    }
    headings.add(hbs[0]);
    bodies.add(hbs[1]);
  }
  print("-------got ${headings.length} headings");

  final pdf = Document();
  final img = await rootBundle.load('assets/cognizant.jpg');
  final imageBytes = img.buffer.asUint8List();

  print("--------creating pdf----");

  pdf.addPage(
    MultiPage(
      pageFormat: PdfPageFormat.a4,
      build: (context) => [
        // page 1 starting------------------------
        Container(
          alignment: Alignment.center,
          height: 200,
          child: Image(MemoryImage(imageBytes)),
        ),
        Center(
          child: Text('BRD Document\n\nSDLC Hackathon\n\n',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 20,
                  decoration: TextDecoration.underline,
                  decorationThickness: 2,
                  fontWeight: FontWeight.bold)),
        ),
        Table(
          border: TableBorder.all(color: PdfColors.black),
          children: [
            TableRow(
              children: [
                Padding(
                  child: Text(
                    "Table of Contents",
                    style: TextStyle(fontSize: 15),
                    textAlign: TextAlign.center,
                  ),
                  padding: EdgeInsets.all(20),
                ),
              ],
            ),

            ...headings.map(
              (e) => TableRow(
                children: [
                  Expanded(
                    child: PaddedText(e),
                    flex: 2,
                  ),
                  Expanded(
                    child: PaddedText(""),
                    flex: 1,
                  )
                ],
              ),
            ),

            TableRow(
              children: [
                PaddedText('Total Sections', align: TextAlign.right),
                PaddedText('${headings.length}'),
              ],
            ),
            TableRow(
              children: [
                PaddedText('Date of creation', align: TextAlign.right),
                PaddedText('${DateTime.now().toIso8601String()}'),
              ],
            ),
            TableRow(
              children: [
                PaddedText('Created by', align: TextAlign.right),
                PaddedText('${'GenXWarriors'}'),
              ],
            ),
            // Show the total
          ],
        ),
// -----------------page 2 starting-------------------

        ListView.builder(
          itemCount: headings.length,
          itemBuilder: (context, i) {
            return Container(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  Text('\n${headings[i]}\n\n',
                      style: TextStyle(
                          fontSize: 16,
                          // color: PdfColor.fromHex("#0000FF"),
                          color: PdfColors.blue500,
                          decoration: TextDecoration.underline,
                          decorationThickness: 2,
                          fontWeight: FontWeight.bold)),
                  Text('${bodies[i]}\n\n')
                ]));
          },
        ),
      ], //here goes the widgets list
    ),
  );
  return pdf;
}

Widget PaddedText(
  final String text, {
  final TextAlign align = TextAlign.left,
}) =>
    Padding(
      padding: EdgeInsets.all(10),
      child: Text(
        text,
        textAlign: align,
      ),
    );
