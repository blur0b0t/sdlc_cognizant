import 'dart:convert';
import 'dart:io';
import 'dart:html' as html;

import 'package:another_flushbar/flushbar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mhi_pred_app/chatbot/models/user_main.dart';
import 'package:mhi_pred_app/chatbot/widgets/UnicornOutlineButton.dart';
import 'package:mhi_pred_app/chatbot/widgets/getPDF.dart';
import 'package:mhi_pred_app/chatbot/widgets/loading_text.dart';
import 'package:mhi_pred_app/utils/data.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../paginate_firestore/paginate_firestore.dart';
import 'package:mhi_pred_app/chatbot/chat_services.dart';
import 'package:mhi_pred_app/chatbot/models/message_model.dart';
import 'package:mhi_pred_app/chatbot/widgets/animated_wave.dart';
import 'package:mhi_pred_app/chatbot/widgets/chat_appbar.dart';
import 'package:mhi_pred_app/chatbot/widgets/messages/text_format.dart';
import 'package:pdf/widgets.dart' as pw;


String lcid = '000';

class ChatWindowPage extends StatefulWidget {
  final UserModel? user;
  final String? docId;

  const ChatWindowPage({Key? key, this.user,this.docId=''}) : super(key: key);

  @override
  _ChatWindowPageState createState() => _ChatWindowPageState();
}

class _ChatWindowPageState extends State<ChatWindowPage> {
  initState() {
    super.initState();
    resumeSession(widget.docId);
    //
    // sendMessage(widget.user!, "context",
    //     "Hi ,Please paste the paragraph in the context window and then type you question in the below text field...", "0");
  }
  void resumeSession(String? docId) async{
    if(docId==''){
      return;
    }

    MessageModel message=await getMessage(widget.user!, docId!);
    setState(() {
    summary = message.summary;
    content = message.context;
    emailTextController.value=TextEditingValue(text:email);
    });

  }

  bool isExpanded = false;
  double composeHeight = 150000;
  TextEditingController messageTextController = new TextEditingController();
  TextEditingController contextTextController =
      new TextEditingController(text: content);
  TextEditingController filePathsTextController = new TextEditingController();
  TextEditingController summaryTextController =
      new TextEditingController(text: summary);
  TextEditingController guestMappingTextController =
      new TextEditingController(text: 'Guest-1m0:Milan(Architect)\nGuest-2m0:Dipankar(Developer)\nGuest-1m1:Jatin(Support)\nGuest-2m1:Jitaditya(QA)');
  TextEditingController emailTextController =      new TextEditingController(text:email);
  List<PlatformFile> files = [];
  bool processingData = false;

  double sheight = 0.0;
  double swidth = 0.0;
  @override
  Widget build(BuildContext context) {
    var collName = 'sdlc_chat';
    double statusBarHeight = MediaQuery.of(context).padding.top;
    double bottomBarHeight = MediaQuery.of(context).padding.bottom;
    // ScreenUtil.init(
    //     BoxConstraints(maxHeight: 650,maxWidth: 330),orientation: Orientation.portrait,designSize: Size(750, 1334)
    // );

    Size size = MediaQuery.of(context).size;
    final width = size.width;
    final height = size.height - statusBarHeight - bottomBarHeight;
    sheight = size.height;
    swidth = size.width;

    // final sheight=ScreenUtil().screenHeight;
    composeHeight = height * 0.1;
    ScrollController _scrollController =
        new ScrollController(keepScrollOffset: true);

    debugPrint(statusBarHeight.toString());
    return Scaffold(
        body: Stack(children: [
      SingleChildScrollView(
          child: Stack(
        children: <Widget>[
          Container(
            height: sheight,
            width: width,
            color: Color.fromRGBO(227, 216, 255, 1),
            child: Column(
              children: <Widget>[
                SizedBox(height: 20),
                Container(height: sheight * 0.13, child: AppBarView()),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(width: 0),
                      Container(
                        width: swidth * 0.4,
                        height: sheight,
                        decoration: BoxDecoration(
                            // color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(40),
                              // topLeft: Radius.circular(40)
                            )),
                        child: Padding(
                            padding: EdgeInsets.only(
                              top: 20,
                            ),
                            child: SizedBox(
                              //height: MediaQuery.of(context).size.height - 250,
                              child: SizedBox(
                                  // width:swidth*0.4,
                                  // height:sheight,

                                  child:
                                      // TextField(
                                      //   maxLines: null,
                                      //   keyboardType: TextInputType.multiline,
                                      //   controller: contextTextController,
                                      //   textCapitalization:
                                      //       TextCapitalization.sentences,
                                      //   onChanged: (value) {},
                                      //   // onSubmitted: (value) {
                                      //   //   messageTextController.clear();
                                      //   //   String context = "context";
                                      //   //   sendMessage(widget.user!, context,
                                      //   //       value, "1");
                                      //   // },
                                      //   decoration: const InputDecoration.collapsed(
                                      //     hintText: 'Enter context',
                                      //   ),
                                      // ),
                                      _buildSummaryWidget()),
                            )),
                      ),
                      const SizedBox(width: 10),
                      Container(
                        height: sheight,
                        width: swidth * 0.59,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(40),
                                topLeft: Radius.circular(40))),
                        child: Column(children: [
                          SizedBox(
                              // width:swidth*0.6,
                              height: sheight * 0.7,
                              child: Padding(
                                  padding: EdgeInsets.only(
                                      top: 20, right: 20, left: 20),
                                  child: PaginateFirestore(
                                    // itemsPerPage: 15,
                                    query: FirebaseFirestore.instance
                                        .collection(collName)
                                        .doc(widget.user!.uid)
                                        // .doc('mhi_pred')

                                        .collection('allMessages')
                                        .orderBy('timestamp', descending: true),
                                    //Change types accordingly
                                    itemBuilderType:
                                        PaginateBuilderType.listView,
                                    // to fetch real-time data
                                    isLive: true,
                                    reverse: true,
                                    // stream: streamMessages(widget.user),
                                    // builder: (context, snapshot) {
                                    //   if (snapshot.hasData) {
                                    //     List<MessageModel> messages =
                                    //         snapshot.data;
                                    //     return ListView.builder(
                                    //       reverse: true,
                                    //       controller: _scrollController,
                                    //       shrinkWrap: true,
                                    //       itemCount: messages.length,
                                    //       itemBuilder: (BuildContext context,
                                    //           int index) {
                                    itemBuilder:
                                        (context, documentSnapshots, index) {
                                      DocumentSnapshot doc =
                                          documentSnapshots[index];
                                      final MessageModel message =
                                          MessageModel.fromFirestore(doc);
                                      final bool isMe = message.senderId ==
                                          widget.user!.name!.split(' ')[0] +
                                              "@red";

                                      if (!isMe &&
                                          message.mCode == '0' &&
                                          processingData == true &&
                                          lcid == message.id) {
                                        print(
                                            "-------------------------setting new content and summary-------------------");

                                        summary = message.summary;
                                        content = message.context;
                                        print(summary);
                                        print(content);
                                        summaryTextController.value =
                                            new TextEditingValue(text: summary);
                                        Future.delayed(Duration.zero, () async {
                                          setState(() {});
                                        });

                                        if (processingData == true) {
                                          Navigator.pop(context);
                                          processingData = false;
                                          Future.delayed(Duration.zero,
                                              () async {
                                            setState(() {});
                                          });
                                        }
                                      }

                                      return TextFormat(message, isMe);
                                    },
                                  ))),
                          SizedBox(
                              width: swidth * 0.6,
                              height: sheight * 0.1,
                              child: _buildMessageComposer()),
                        ]),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ))
    ]));
  }

  _buildMessageComposer() {
    // return Consumer<ReplyProvider>(
    //   builder: (context, value, child) {
    // if (value.replyMessage.isText ||
    //     value.replyMessage.isDocument ||
    //     value.replyMessage.isAudio ||
    //     value.replyMessage.isContact ||
    //     value.replyMessage.isVideo) {
    //   if(composeHeight==70 ||composeHeight==180){
    //   setState(() {
    //     composeHeight=composeHeight+100;
    //   });
    //   }
    // }

    return AnimatedContainer(
      duration: Duration(milliseconds: 250),
      color: Colors.white,
      height: composeHeight,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 250),
        decoration: BoxDecoration(
            color: Color.fromRGBO(243, 242, 247, 1),
            //Color.fromRGBO(42, 15, 113, 1),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            )),
        padding: EdgeInsets.symmetric(horizontal: 0.0),
        height: composeHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: TextField(
                        controller: messageTextController,
                        textCapitalization: TextCapitalization.sentences,
                        onChanged: (value) {},
                        onSubmitted: (txt) {
                          messageTextController.clear();
                          // String context = contextTextController.text;
                          sendMessage(widget.user!,
                              context: content, txt: txt, mCode: "1");
                        },
                        decoration: const InputDecoration.collapsed(
                          hintText: 'Type a message',
                        ),
                      ),
                    ),
                    IconButton(
                      icon: SvgPicture.asset("assets/svgs/send.svg",
                          height: 23,
                          color: const Color.fromRGBO(42, 15, 113, 1),
                          semanticsLabel: 'A red up arrow'),
                      iconSize: 25.0,
                      onPressed: () {
                        String value = messageTextController.text;
                        // String context= contextTextController.text;
                        // String context = contextTextController.text;
                        messageTextController.clear();
                        sendMessage(widget.user!,
                            context: content, txt: value, mCode: "1");
                      },
                    ),
                  ],
                ),
                Positioned(
                  bottom: 0.0,
                  left: 0.0,
                  right: 0.0,
                  child: AnimatedWave(
                    height: 10,
                    speed: 1.5,
                  ),
                ),
                Positioned(
                  bottom: 0.0,
                  left: 0.0,
                  right: 0.0,
                  child: AnimatedWave(
                    height: 10,
                    speed: 1.3,
                    offset: 3.14,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _buildSubmitButton() {
    return AnimatedContainer(
      duration: Duration(milliseconds: 250),
      color: Color.fromRGBO(227, 216, 255, 1),
      height: composeHeight,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 250),
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        height: composeHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // SizedBox(width: 0,),

                    Container(
                      alignment: Alignment.center,
                      // padding: EdgeInsets.all(30),
                      // width:swidth*0.2,
                      child: UnicornOutlineButton(
                        strokeWidth: 20,
                        radius: 24,
                        gradient: LinearGradient(
                          colors: [
                            Colors.pinkAccent.shade100,
                            Colors.blueAccent.shade100
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        ),
                        child: Text('      Process Files      ',
                            style: TextStyle(fontSize: 30)),
                        onPressed: () async {
                          if (files.isEmpty) return;
                          showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (context) {
                                return LoadingText(
                                    loadingText: "Uploading Data");
                              });
                          List<String> upFileUrls =
                              files == [] ? [] : await uploadFiles(files);
                          if (upFileUrls.isEmpty) return;

                          lcid = await sendMessage(widget.user!,
                              fileUrls: upFileUrls,
                              txt: "process the files",
                              mCode: "0",
                              email:emailTextController.text);

                          Navigator.pop(context);

                          print("data uploaded-------------------");
                          Flushbar(
                            title: 'Data Uploaded',
                            message: 'Data Uploaded successfully',
                            duration: Duration(seconds: 3),
                          ).show(context);

                          // setState(() {
                          processingData = true;
                          // });
                          showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (context) {
                                return LoadingText(
                                    loadingText:
                                        "Processing files using Azure AI Services");
                              });
                        },
                      ),
                    ),
                  ],
                ),
                // Positioned(
                //   bottom: 0.0,
                //   left: 0.0,
                //   right: 0.0,
                //   child: AnimatedWave(
                //     height: 10,
                //     speed: 1.5,
                //   ),
                // ),
                // Positioned(
                //   bottom: 0.0,
                //   left: 0.0,
                //   right: 0.0,
                //   child: AnimatedWave(
                //     height: 10,
                //     speed: 1.3,
                //     offset: 3.14,
                //   ),
                // ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  showSummary(String summary) {
    showDialog(
        context: context,
        // barrierColor: Colors.white,
        builder: (BuildContext context) => Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
            elevation: 16,
            child: Container(
              decoration: BoxDecoration(
                // border: Border.all(
                //   color: Colors.red[500],
                // ),
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              // color: Colors.white,
              height: sheight,
              width: swidth * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: sheight * 0.2,
                    width: swidth,
                    child: ListView.builder(
                        physics: ClampingScrollPhysics(),
                        itemCount: 3,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index) {
                          return Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Container(
                                      width: index == 1 ? 150.0 : 100.0,
                                      height: index == 1 ? 200.0 : 60.0,
                                      child: Image.asset(
                                        index == 0
                                            ? "assets/azure.png"
                                            : index == 1
                                                ? "assets/cognizant.jpg"
                                                : "assets/azure.png",
                                        fit: BoxFit.cover,
                                      ))),
                            ),
                          );
                        }),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    padding: EdgeInsets.all(
                      20,
                    ),
                    width: swidth * 0.4,
                    height: sheight * 0.65,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(40))),
                    child: Padding(
                      padding: EdgeInsets.only(
                        top: 20,
                      ),
                      child: SizedBox(
                        width: swidth * 0.3,
                        height: sheight * 0.5,
                        child: TextFormField(
                          // initialValue: summary,
                          maxLines: null,
                          keyboardType: TextInputType.multiline,
                          controller: summaryTextController,
                          textCapitalization: TextCapitalization.sentences,
                          onChanged: (value) {},

                          decoration: const InputDecoration.collapsed(
                            // border: InputBorder(borderRadius: BorderRadius.all(Radius.circular(40))),
                            hintText: 'Summary will appear here...',
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )));
  }

  Future<Map<String, String>> guestMappingDialogue() async {
    await showDialog(
        context: context,
        // barrierColor: Colors.white,
        builder: (BuildContext context) => Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
            elevation: 16,
            child: Container(
                decoration: BoxDecoration(
                  // border: Border.all(
                  //   color: Colors.red[500],
                  // ),
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                // color: Colors.white,
                height: sheight*0.4,
                width: swidth * 0.5,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        height: sheight * 0.3,
                        width: swidth,
                        child: Container(
                          padding: EdgeInsets.all(
                            20,
                          ),
                          width: swidth * 0.4,
                          height: sheight * 0.65,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(40))),
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: 20,
                            ),
                            child: SizedBox(
                              width: swidth * 0.3,
                              height: sheight * 0.5,
                              child: TextFormField(
                                // initialValue: summary,
                                maxLines: null,
                                keyboardType: TextInputType.multiline,
                                controller: guestMappingTextController,
                                textCapitalization:
                                    TextCapitalization.sentences,
                                onChanged: (value) {},

                                decoration: const InputDecoration.collapsed(
                                  // border: InputBorder(borderRadius: BorderRadius.all(Radius.circular(40))),
                                  hintText: 'Enter speaker/guest mapping here',
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ]))));
          List<String> gmapt=guestMappingTextController.text.split('\n');
          Map<String,String> gmap={};
          for(String gm in gmapt){
            List<String> gml=gm.split(':');
            gmap[gml[0]]=gml[1];
          }
          return gmap;

  }

  downloadSummary(summary) async {
    print("-------------download summary pressed---------------");
    // final pdf = pw.Document();
    // final img = await rootBundle.load('assets/cognizant.jpg');
    // final imageBytes = img.buffer.asUint8List();

    // pdf.addPage(
    //   pw.Page(
    //     build: (pw.Context context) => pw.Center(
    //         child: pw.Column(children: [
    //       pw.Container(
    //         alignment: pw.Alignment.center,
    //         height: 200,
    //         child: pw.Image(pw.MemoryImage(imageBytes)),
    //       ),
    //       pw.Text('Summary:\n\n\n'),
    //       pw.Text(summary),
    //     ])),
    //   ),
    // );
    pw.Document pdf = await getPDF(summary);
    //Create PDF in Bytes
    Uint8List pdfInBytes = await pdf.save();
    print("--------pdf file created---------------");

    //Create blob and link from bytes
    final blob = html.Blob([pdfInBytes], 'application/pdf');
    final url = html.Url.createObjectUrlFromBlob(blob);
    final anchor = html.document.createElement('a') as html.AnchorElement
      ..href = url
      ..style.display = 'none'
      ..download = 'summary.pdf';
    html.document.body?.children.add(anchor);
    anchor.click();

    //   final file = PlatformFile(name: 'summary.pdf');
    //   await file..writeAsBytes(await pdf.save());
    // launchUrl(Uri.parse(
    //     "data:application/octet-stream;base64,${base64Encode(pdfInBytes)}"));
  }

  _buildSummaryWidget() {
    return Container(
      height: sheight,
      width: swidth * 0.4,
      color: Color.fromRGBO(227, 216, 255, 1),
      child: Column(
        children: <Widget>[
          // SizedBox(height: 20),
          // Container(height: sheight * 0.13, child: AppBarView()),
          // Container(height: sheight * 0.13, child: AppBarView()),
          
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //  SizedBox(
                //   height: 5,
                // ),
                // Text("userEmail will come here..."),
                Container(
                  padding: EdgeInsets.all(
                        0,
                      ),
                      width: swidth * 0.34,
                      // height: sheight * 0.30,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
              child: TextFormField(
                controller: emailTextController,
                style: TextStyle(
                  fontSize: 22,
                  // color: Colors.blue,
                  // fontWeight: FontWeight.w600,
                ),
                // onChanged: (value) {
                //   setState(() {
                //     emailTextController.text = value.toString();
                //   });
                // },
                decoration: InputDecoration(
                  fillColor:Colors.white,
                  focusColor: Colors.white,
                  //add prefix icon
                  prefixIcon: Icon(
                    Icons.email_outlined,
                    color: Colors.red,
                  ),


                  // border: OutlineInputBorder(
                  //   borderRadius: BorderRadius.circular(10.0),
                  // ),

                  // focusedBorder: OutlineInputBorder(
                  //   borderSide:
                  //       const BorderSide(color: Colors.blue, width: 1.0),
                  //   borderRadius: BorderRadius.circular(10.0),
                  // ),

                  hintText: "Enter your email to receive notification",

                  //make hint text
                  hintStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 16,
                    fontFamily: "verdana_regular",
                    fontWeight: FontWeight.w400,
                  ),

                  //create lable
                  labelText: 'Email',
                  //lable style
                  labelStyle: TextStyle(
                    color: Colors.grey,
                    fontSize: 16,
                    fontFamily: "verdana_regular",
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
                SizedBox(
                  height: 15,
                ),
                // file upload widget
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.all(
                        10,
                      ),
                      width: swidth * 0.34,
                      height: sheight * 0.30,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(40))),
                      child: Padding(
                          padding: EdgeInsets.only(
                            top: 20,
                          ),
                          child: Row(children: [
                            SizedBox(
                              width: swidth * 0.25,
                              height: sheight * 0.5,
                              child: ReorderableListView( 
        onReorder: (int oldindex, int newindex) {
    setState(() {
      if (newindex > oldindex) {
        newindex -= 1;
      }
      final items = filePaths.removeAt(oldindex);
         filePaths.insert(newindex, items);
    });},
        children: [
          for (final items in filePaths)
            Card(
              color: Colors.white,
              key: ValueKey(items),
              elevation: 2,
              child: ListTile(
                title: Text(items),
                 
              ),
            ),
        ],
        ),
                              // TextField(
                              //   maxLines: null,
                              //   keyboardType: TextInputType.multiline,
                              //   controller: filePathsTextController,
                              //   textCapitalization:
                              //       TextCapitalization.sentences,
                              //   onChanged: (value) {},
                              //   decoration: const InputDecoration.collapsed(
                              //     // border: InputBorder(borderRadius: BorderRadius.all(Radius.circular(40))),
                              //     hintText:
                              //         'Selected files to upload will appear here',
                              //   ),
                              // ),
                            ),
                            SizedBox(width:swidth*0.01 ,),
                            IconButton(
                                onPressed: () async {
                                  FilePickerResult? result = await FilePicker
                                      .platform
                                      .pickFiles(allowMultiple: true);

                                  if (result != null) {
                                    // files = result.paths
                                    //     .map((path) => File(path!))
                                    //     .toList();
                                    files = result.files;
                                    print(files.length);
                                    filePaths=[];
                                    files.forEach((f) {
                                      filePaths.add(f.name);
                                    });
                                    print(
                                        "------------------------------------\n got file:${filePaths}");
                                    filePathsTextController.value =
                                        TextEditingValue(
                                            text: filePaths.join('\n'));
                                    setState(() {});
                                  } else {
                                    // User canceled the picker
                                  }
                                },
                                icon: Icon(
                                  Icons.cloud_upload,
                                  size: swidth * 0.05,
                                  color: Colors.blue,
                                ))
                            // ElevatedButton(child:Text("upload files"),onPressed: (){},),
                          ])),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    // Container(
                    //   padding: EdgeInsets.all(
                    //     20,
                    //   ),
                    //   width: swidth * 0.3,
                    //   height: sheight * 0.35,
                    //   decoration: BoxDecoration(
                    //       color: Colors.white,
                    //       borderRadius:
                    //           BorderRadius.all(Radius.circular(40))),
                    //   child: Padding(
                    //       padding: EdgeInsets.only(
                    //         top: 20,
                    //       ),
                    //       child: Row(children: [
                    //         SizedBox(
                    //           width: swidth * 0.27,
                    //           height: sheight * 0.5,
                    //           child: TextField(
                    //             maxLines: null,
                    //             keyboardType: TextInputType.multiline,
                    //             controller: fileUrlsTextController,
                    //             textCapitalization:
                    //                 TextCapitalization.sentences,
                    //             onChanged: (value) {},
                    //             decoration:
                    //                 const InputDecoration.collapsed(
                    //               // border: InputBorder(borderRadius: BorderRadius.all(Radius.circular(40))),
                    //               hintText:
                    //                   'Add urls of files that need to be processed',
                    //             ),
                    //           ),
                    //         ),
                    //       ])),
                    // ),
                  ],
                ),
                SizedBox(
                  height: sheight*0.01,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.deepOrange,
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                  width: swidth * 0.35,
                  child: _buildSubmitButton(),
                ),
                const SizedBox(height: 10),
                Container(
                    // height: sheight*0.18,
                    width: swidth * 0.35,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(40))),
                    child: Column(children: [
                      Container(
                        padding: const EdgeInsets.all(10),
                        width: swidth * 0.6,
                        // height: sheight * 0.22,
                        child: Column(children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                FloatingActionButton.extended(
                                  label: Text(
                                    'View Summary',
                                    style: TextStyle(color: Colors.white),
                                  ), // <-- Text
                                  backgroundColor: Colors.blue,
                                  icon: Icon(
                                    // <-- Icon
                                    Icons.remove_red_eye,
                                    size: 50.0,
                                    color: Colors.white,
                                  ),
                                  onPressed: () {
                                    showSummary(summary);
                                  },
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                FloatingActionButton.extended(
                                  label: Text(
                                    'Download Summary (PDF)',
                                    style: TextStyle(color: Colors.white),
                                  ), // <-- Text
                                  backgroundColor: Colors.blue,
                                  icon: Icon(
                                    // <-- Icon
                                    Icons.download,
                                    size: 50.0,
                                    color: Colors.white,
                                  ),
                                  onPressed: () {
                                    downloadSummary(summary);
                                  },
                                ),
                              ]),
                              SizedBox(height: sheight*0.03,),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                FloatingActionButton.extended(
                                  label: Text(
                                    'Add speaker mapping',
                                    style: TextStyle(color: Colors.white),
                                  ), // <-- Text
                                  backgroundColor: Colors.blue,
                                  icon: Icon(
                                    // <-- Icon
                                    Icons.remove_red_eye,
                                    size: 50.0,
                                    color: Colors.white,
                                  ),
                                  onPressed: () async {
                                    Map<String,String> gmap=await guestMappingDialogue();
                                    debugPrint("--------------rec gmap--------------");
                                    print(gmap);
                                    gmap.forEach((key, value) {
                                    summary.replaceAll(key, value);                        
                                    });
                                    
                                  },
                                ),
                              ]),
                        ]),
                      ),
                    ]))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
