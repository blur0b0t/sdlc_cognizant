import 'dart:convert';
// import 'dart:ffi';
import 'dart:js_interop';
import 'dart:io';
import 'dart:typed_data';
import 'dart:io';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:mhi_pred_app/chatbot/models/user_main.dart';
import 'package:mhi_pred_app/main.dart';
import './models/message_model.dart';
import 'package:http/http.dart' as http;
import 'package:azblob/azblob.dart';
import 'package:mime/mime.dart';


var _db = FirebaseFirestore.instance;
FirebaseStorage _storage = FirebaseStorage.instance;


Future<String> uploadAzureBlob(PlatformFile file) async {
  String fname=file.name;
  Uint8List fbytes=file.bytes as Uint8List;

  print("-----------starting upload to azure blob stoarge----$fname-----------");


  String acs="DefaultEndpointsProtocol=https;AccountName=genxwarriors;AccountKey=gU8JdYh9K/EUd7MTTLFngcijYdmZp13HDktWT0ywn8dIxpna5UQZ4YqcYAASxegrUfQdi0vmiNUm+AStGSd+3g==;EndpointSuffix=core.windows.net";
  // String acs="https://genxwarriors.blob.core.windows.net/input-files?sp=racwdli&st=2024-05-19T09:59:13Z&se=2024-09-07T17:59:13Z&spr=https&sv=2022-11-02&sr=c&sig=REdnxKBGnPtDV%2F2Yp1pqa11N2C0vPEFkoS1gYEG2Gm0%3D";
  var blobPath='/input-files/$fname';
  var storage = AzureStorage.parse(acs);
  String? contentType= lookupMimeType(fname);
  await storage.putBlob(blobPath,bodyBytes: fbytes,contentType: contentType,type: BlobType.blockBlob);
  print("done");

  // await storage.putBlob(blobPath,
  //   bodyBytes: fbytes);
  print("----------uploaded to azure------------------");

  return blobPath;
}

Future<List<String>> uploadFiles(List<PlatformFile> files) async {
  List<String> fileUrls = [];
  for (PlatformFile file in files) {
    // azure blob stoarge
    // String fileUrl =await uploadAzureBlob(file);

    //Upload the file to firebase
    //Create a reference to the location you want to upload to in firebase
    String filePath = file.name;
    Reference reference =
        _storage.ref().child("sdlc_cognizant/${filePath.split('/').last}");
    UploadTask uploadTask = reference.putData(file.bytes as Uint8List);
    // Waits till the file is uploaded then stores the download url
    String fileUrl =await (await uploadTask.whenComplete(() => null)).ref.getDownloadURL();

    print("upoalded file url=$fileUrl");
    fileUrls.add(fileUrl);
  }
  //returns the download url
  return fileUrls;
}

Stream<List<MessageModel>> streamMessages(UserModel user) {
  var ref = _db.collection(coll_name).doc(user.uid).collection('allMessages');
  // int len=await ref.snapshots().length;
  // debugPrint(len);
  return ref.snapshots().map((list) => list.docs.map((doc) {
        debugPrint(doc.data().toString());
        return MessageModel.fromFirestore(
          doc,
        );
      }).toList());
//   return const Stream.empty();
}

Future<MessageModel> getMessage(UserModel user,String docId) async{
  var ref = _db.collection(coll_name).doc(user.uid).collection('allMessages').doc(docId);

  return MessageModel.fromFirestore(await ref.get());
//   return const Stream.empty();
}

Future<String> sendMessage(UserModel user,
    {fileUrls = const [],
    context = '',
    String txt = '',
    String mCode = "1",
    String rType = '1',
    String email=''}) async {
  if (txt.isEmpty) return '000';
  MessageModel message = MessageModel(timestamp: Timestamp.now());
  message.id = Timestamp.now().millisecondsSinceEpoch.toString();
  message.senderId =
      rType == '0' ? "chatbot@red" : "${user.name?.split(' ')[0]}@red";
  message.rType = rType;
  message.timestamp = Timestamp.now();
  message.message = txt;
  message.context = context;
  message.fileUrls = List<String>.from(fileUrls);
  message.mCode = mCode;
  message.email=email;

  await _db
      .collection(coll_name)
      .doc(user.uid)
      .collection('allMessages')
      .doc(message.timestamp.millisecondsSinceEpoch.toString())
      .set(message.getMap());

  // -------------chatbot response
  if (rType == '0') return message.id;
  await _db
      .collection(coll_name)
      .doc(user.uid)
      .collection('userMessages')
      .doc('message')
      .set(message.getMap());
  return message.id;

  // ----------------------from python api----------------
  //
  // final queryParameters = {
  //   'input': context,
  //   'instruction': txt,
  // };
  // print("-----------generating uri--------------");
  // final uri = Uri.http('127.0.0.1:5000', '/predict', queryParameters);
  // print("-----------calling api------------------");
  // final response = await http.get(
  //   uri,
  // );
  // print("-----------api call succesfull------------------");
  //
  //
  // txt = jsonDecode(response.body)['output'];
  //
  // // txt='chatbot respone.......';
  // message = MessageModel(timestamp: Timestamp.now());
  // message.senderId = 'chatbot' + "@red";
  // message.rType = rType;
  // message.timestamp = Timestamp.now();
  // message.message = txt;
  //
  // _db
  //     .collection(coll_name)
  //     .doc(user.uid)
  //     .collection('allMessages')
  //     .doc(message.timestamp.millisecondsSinceEpoch.toString())
  //     .set(message.getMap());
}
