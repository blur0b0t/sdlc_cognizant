
String email='jatin.yadav3@cognizant.com';

String content = """
At Cognizant, we have been on a quest to advance AI beyond existing techniques, by taking a more holistic,human-centric approach to learning and understanding. As Chief Technology Officer of Azure AI Services, 
I have been working with a team of amazing scientists and engineers to turn this quest into a reality. In my role, I enjoy a unique perspective in viewing the relationship among three attributes of human cognition:
monolingual text (X), audio or visual sensory signals, (Y) and multilingual (Z). At the intersection of all three,there’s magic—what we call XYZ-code as illustrated in Figure 1—a joint representation to create more powerful AI that can speak,
hear, see, and understand humans better. We believe XYZ-code will enable us to fulfill our long-term vision: cross-domain transfer learning, spanning modalities and languages. The goal is to have pre-trained models that can jointly learn representations to support a broad range of downstream AI tasks, much in the way humans do today. Over the past five years, we have achieved human performance on benchmarks in conversational speech recognition, machine translation, conversational question answering, machine reading comprehension, and image captioning. These five breakthroughs provided us with strong signals toward our more ambitious aspiration to produce a leap in AI capabilities, achieving multi-sensory and multilingual learning that is closer in line with how humans learn and understand. I believe the joint XYZ-code is a foundational component of this aspiration, if grounded with external knowledge sources in the downstream AI tasks.
""";

// String summary = "please process the files first to generate summary...";

String summary = """
summary:

1) Introduction: The team discussed the SDLC hackathon requirement and the goal of building a system that can generate a BRD document automatically.
2) Requirements Overview: The application will be built using Azure subscription and will be used for the requirement gathering phase of the development cycle.
3) In scope: The application will support various file formats such as video, audio, email communication, text, word, and PDF.
4) Out of scope: The initial phase will focus on the U.S. market, with plans to expand to other markets later.
5) Assumptions: The team assumes that the application will save time and reduce manual errors in preparing requirement documents.
6) Notes: The team also discussed the possibility of incorporating a bot to clarify and ask further questions about the content uploaded by the user, as well as implementing measures to restrict inappropriate inputs.

Content Filter results=[{'prompt_index': 0, 'content_filter_results': {'hate': {'filtered': False, 'severity': 'safe'},'self_harm': {'filtered': False, 'severity': 'safe'}, 'sexual': {'filtered': False, 'severity': 'safe'},'violence': {'filtered': False, 'severity': 'safe'}}}]
""";

List<String> filePaths= ['meeting1.mp4','meeting2.mp4','doc1.txt'];