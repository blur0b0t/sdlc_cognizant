import smtplib

def sendEmail(link,email):
    s = smtplib.SMTP('smtp.gmail.com', 587)
    fromEmail="doc_generator@cognizant.com"
    s.starttls()
    s.login(fromEmail, "password")
    message = f"""
    Hi, the files are processed.
    Now you can download the BRD document or ask questions from the processed files.

    click on the below link to continue session:
    {link}

    """
    s.sendmail(fromEmail, email, message)
    s.quit()