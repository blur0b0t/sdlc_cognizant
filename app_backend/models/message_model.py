class MessageModel(object):
    def __init__(self, id, senderId,mCode,rType,message,context,summary,fileUrls,timestamp,output,ots,pfr,isFlagged,email):
        self.id = id
        self.senderId = senderId
        self.mCode=mCode
        self.rType=rType
        self.message=message
        self.context = context
        self.summary = summary
        self.fileUrls = fileUrls
        self.timestamp = timestamp
        self.output=output
        self.ots=ots
        self.pfr=pfr
        self.isFlagged=isFlagged
        self.email=email
    
    def from_dict(data):
        return MessageModel(
            id=data.get('id'),
            senderId=data.get('senderId'),
            mCode=data.get('mCode'),
            rType=data.get('rType'),
            message=data.get('message'),
            context=data.get('context'),
            summary=data.get('summary'),
            fileUrls=data.get('fileUrls'),
            timestamp=data.get('timestamp'),
            output=data.get('output'),
            ots=data.get('ots'),
            pfr=data.get('pfr'),
            isFlagged=data.get('isFlagged'),
            email=data.get('email'),

            )
    
